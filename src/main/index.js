import { app, BrowserWindow } from 'electron'

if(require('./squirrel')) app.quit();

const fs = require('fs')
const path = require('path')
const folders = ['logs', 'db'];

if (process.env.NODE_ENV === 'production') {
  try {
    fs.mkdirSync(path.join(app.getPath("documents"), 'OSMecanica'));
  
    folders.forEach(f => {
      fs.mkdirSync(path.join(app.getPath("documents"), 'OSMecanica', f));
    });
  } catch (e) {
    if (e.code !== 'EEXIST') throw e;
  }
} else {
  try {  
    folders.forEach(f => {
      fs.mkdirSync(f);
    });
  } catch (e) {
    if (e.code !== 'EEXIST') throw e;
  }
}

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow;

const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */

  let windowopts = {
    useContentSize: true
  }

  if (process.env.NODE_ENV === 'development') {
    windowopts.width = 1366;
    windowopts.height = 685;
  }

  mainWindow = new BrowserWindow(windowopts)

  mainWindow.loadURL(winURL)
  mainWindow.setMenuBarVisibility(false)

  if (process.env.NODE_ENV === 'production') {
    mainWindow.maximize()
  }

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

let shouldQuit = app.makeSingleInstance((cmd, cwd) => {
  if (mainWindow) {
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore();
      mainWindow.focus();
    }
  }
});

if (shouldQuit) {
  app.quit();
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})