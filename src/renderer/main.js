import Vue from 'vue'

import App from './App'
import router from './router'

import "./assets/css/materialdesignicons.min.css"

// import { remote } from 'electron'

// remote.globalShortcut.register('CommandOrControl+Shift+K', () => {
//   remote.BrowserWindow.getFocusedWindow().webContents.openDevTools()
// })

// window.addEventListener('beforeunload', () => {
//   remote.globalShortcut.unregisterAll()
// })

import logger from "./services/utils/logger";
const error = console.error.bind(console)

/* Toda vez que console.error for usado, guardar no arquivo de log */
console.error = (...args) => {
  logger(args);
  error(...args);
}

import Buefy from 'buefy'
Vue.use(Buefy)

import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)

import money from 'v-money'
Vue.use(money, {precision: 2})

import Select from './components/ui/Select'
Vue.component('c-select', Select)

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.config.productionTip = false

import ClienteService from "./services/ClienteService"
import VeiculoService from "./services/VeiculoService"
import OrdemServicoService from "./services/OrdemServicoService"
import ConfigService from './services/ConfigService'

Vue.prototype.$service = {
  clientes: new ClienteService(),
  veiculos: new VeiculoService(),
  os:       new OrdemServicoService(),
  settings: new ConfigService()
}

Vue.prototype.$log = logger;

import { formatCpfCnpj, formatCurrency, formatPlate, formatTel } from "./services/utils/utils";

Vue.filter('formatTel', formatTel)
Vue.filter('formatCpfCnpj',  formatCpfCnpj)
Vue.filter('formatPlate', formatPlate)
Vue.filter('formatCurrency', formatCurrency)

logger("Iniciando aplicação");

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  template: '<App/>',
  methods: {
    getVersion() {
      return require("../../package.json").version;
    },
    success(text, duration = 5000) {
      this.$buefy.notification.open({ message: text, type: 'is-success', duration: duration, position: 'is-bottom-right', queue: false, hasIcon: true });
    },
    error(text, duration = 5000) {
      this.$buefy.notification.open({ message: text, type: 'is-danger', duration: duration, position: 'is-bottom-right', queue: false, hasIcon: true });
    },
    alert(type, title, withIcon, icon, message) {
      return new Promise(
        (resolve) => {
          this.$buefy.dialog.alert({
            type: type, title: title, hasIcon: withIcon, icon: icon, message: message,
            onConfirm: () => { resolve() }
          });
        }
      )
    },
    confirm(type, title, message) {
      return new Promise(
        (resolve) => {
          this.$buefy.dialog.confirm({
            type: type, title: title, message: message, confirmText: 'Sim', cancelText: 'Não',
            onConfirm: () => { resolve(true) },
            onCancel: () => { resolve(false) }
          });
        }
      )
    },
  }
}).$mount('#app')
