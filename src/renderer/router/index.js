import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: require('@/components/Index').default
    },
    {
      path: '/cliente/new',
      name: 'new-cliente',
      component: require('@/components/cliente/EditCliente').default
    },
    {
      path: '/cliente/:id',
      name: 'edit-cliente',
      component: require('@/components/cliente/EditCliente').default
    },
    {
      path: '/cliente',
      name: 'list-cliente',
      component: require('@/components/cliente/ListClientes').default
    },
    {
      path: '/os/new',
      name: 'new-os',
      component: require('@/components/os/EditOrdemServico').default
    },
    {
      path: '/os/:id',
      name: 'edit-os',
      component: require('@/components/os/EditOrdemServico').default
    },
    {
      path: '/os',
      name: 'list-os',
      component: require('@/components/os/ListOrdemServico').default
    },
    {
      path: '/veiculo',
      name: 'list-veiculo',
      component: require('@/components/veiculo/ListVeiculos').default
    },
    {
      path: '/settings',
      name: 'configuracoes',
      component: require('@/components/Config').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
