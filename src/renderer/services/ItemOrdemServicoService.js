import db from "./db/itemos";
import logger from "./utils/logger";

const fuzzysort = require('fuzzysort');

export default class ItemOrdemServicoService {

    constructor() {
        this.db = db;
    }

    getItemsByOs(id) {
        return new Promise(
            (resolve, reject) => {
                this.db.find(
                    { id_os: id },
                    (error, data) => {
                        if (error) {
                            logger(`Erro ao carregar itens da OS [${id}]`, e);
                            reject(error);
                        }

                        resolve(data);
                    }
                );
            }
        );
    }

    insertItems(id_os, items) {
        return new Promise(
            (resolve, reject) => { 
                if (!id_os) {
                    logger(`Erro ao inserir itens na OS [${id_os}]: ID da OS não existe`);
                    reject({errorType: 'noIdSpecified' });
                }

                if (Array.isArray(items)) {
                    let item_array = [];

                    items.forEach(i => {
                        const {qtd, descricao, valorunit} = i;

                        item_array.push({
                            id_os:      id_os,
                            qtd:        qtd,
                            descricao:  descricao,
                            valorunit:  valorunit
                        });
                    });

                    this.db.insert(item_array, (error, data) => {
                        if (error) {
                            logger(`Erro ao inserir itens na OS [${id_os}]`, error);
                            reject(error);
                        }

                        logger(`Itens inseridos com sucesso na OS [${id_os}]`);
                        resolve(data);
                    });                    
                } else {
                    logger(`Erro ao inserir itens na OS [${id_os}]: Data is not array. Conteúdo:`, items);
                    reject({ errorType: 'typeError' });
                }
            }
        );
    }

    updateItems(id_os, newitems) {
        return new Promise(
            (resolve, reject) => {
                if (!id_os) {
                    logger(`Erro ao atualizar itens na OS [${id_os}]: ID da OS não existe`);
                    reject({errorType: 'itemOS_noIdSpecified' });
                }

                this.db.remove({ id_os: id_os }, { multi: true }, function (error, qtdRemoved) {
                    if (error) { 
                        logger(`Erro ao remover itens na OS [${id_os}] para atualização`, error);
                        reject(error)
                    }
                });

                this.insertItems(id_os, newitems)
                    .then(items => {
                        logger(`Itens da OS [${id_os}] atualizados com sucesso`);
                        resolve(items);
                    })
                    .catch(error => {
                        logger(`Erro ao atualizar itens na OS [${id_os}]`, error);
                        reject(error);
                    })
            }
        );
    }

    removeAllByOs(id) {
        return new Promise(
            (resolve, reject) => {
                if (!id) {
                    logger(`Erro ao remover itens na OS [${id_os}]: ID da OS não existe`);
                    reject({errorType: 'noIdGiven'});
                }
                
                this.db.remove({ id_os: id }, { multi: true }, (err, qtd) => {
                    if (err) {
                        logger(`Erro ao remover itens na OS [${id_os}]`, error);
                        reject(err)
                    };

                    logger(`Itens da OS [${id_os}] removidos com sucesso`);
                    resolve(qtd);
                })
            }
        );
    }

    searchItens(query) {
        return new Promise(
            (resolve, reject) => {
                this.db.find(
                    {}, { descricao: 1, valorunit: 1, _id: 0 },
                    (err, data) => {
                        try {
                            if (err) {
                                throw err
                            }
    
                            let res = fuzzysort.go(query, data, { key: 'descricao' })
                                .map(e => {
                                    let obj = e.obj;
                                    obj.highlighted = fuzzysort.highlight(e, '<b>','</b>');
                                    return obj;
                                });
                                
                            resolve(res.map(e => e.descricao).map((e, i, f) => f.indexOf(e) === i && i).filter(e => res[e]).map(e => res[e]));
                        } catch (e) {
                            logger(`Erro ao pesquisar itens de OS`, e);
                            reject(e);
                        }
                    }
                )
            }
        );
    }

}