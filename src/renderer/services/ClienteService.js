import db from "./db/clientes";
import veiculosDb from "./db/veiculos";
import osDb from "./db/os";

import logger from "./utils/logger";

export default class Clientes {

    constructor() {
        this.db = db;
    }

    get(id, vehicles = true) {
        return new Promise(
            (resolve, reject) => {
                db.findOne(
                    {_id: id}, 
                    (e, d) => {
                        if (e) {
                            logger(`Erro ao carregar cliente [${id}]`, e);
                            reject(e);
                        } 

                        if (vehicles) {
                            veiculosDb.find(
                                { id_cliente: id }, 
                                (ev, dv) => {
                                    if (ev) { 
                                        logger(`Erro ao carregar veículos do cliente [${id}]`, e);
                                        reject(ev)
                                    }
                                    d.veiculos = dv;
                                }
                            );
                        }

                        resolve(d);
                    }
                )
            }
        );
    }

    findAll(query = '', page = 1, size = 20, column = 'createdAt', order = 'asc') {
        /* Sanitize query */
        let sq = (typeof query == 'string') ? query.toLocaleLowerCase().trim().replace(/[-|/|.|(|)]/g, '') : '';

        /* Sanitize page */
        let sp = Number(page); sp = isNaN(sp) ? 1 : (sp < 1 ? 1 : sp);

        /* Sanitize size */
        let ss = Number(size); ss = isNaN(ss) ? 20 : (ss < 1 ? 1 : ss);

        /* Sanitize column */
        let sc = (typeof column == 'string') ? column.toLocaleLowerCase().trim() : 'createdAt';

        /* Sanitize order */
        let so = order == 'asc' ? 1 : -1;

        return new Promise(
            (resolve, reject) => {
                db.find({
                    $where: function() {
                        let nome = this.nome ? this.nome.toLocaleLowerCase() : '';
                        let rsocial = this.rsocial ? this.rsocial.toLocaleLowerCase() : '';
                        let s = `${(nome)} ${rsocial} ${this.telefone || ''} ${this.celular || ''} ${this.cnpj_cpf || ''}`;

                        return s.includes(sq);
                    }
                }, (e, d) => {
                    if (e) { 
                        logger(`Erro ao carregar clientes`, e);
                        reject(e)
                    };
                    
                    let count = d.length;

                    /* Sort */
                    if (sc != 'createdAt') {
                        if (so > 0) {   /* ASC */
                            resolve(
                                {
                                    content: d.sort((a, b) => a[sc].toLocaleLowerCase().localeCompare(b[sc].toLocaleLowerCase())).splice((sp - 1) * ss, ss),
                                    total: count
                                }
                            );
                        } else {        /* DESC */
                            resolve(
                                {
                                    content: d.sort((a, b) => b[sc].toLocaleLowerCase().localeCompare(a[sc].toLocaleLowerCase()))
                                              .splice((sp - 1) * ss, ss),
                                    total: count 
                                }
                            );
                        }
                    } else {
                        if (so > 0) {   /* ASC */
                            resolve(
                                {
                                    content: d.sort((a, b) => a.createdAt - b.createdAt).splice((sp - 1) * ss, ss),
                                    total: count
                                }
                            );
                        } else {        /* DESC */
                            resolve(
                                {
                                    content: d.sort((a, b) => b.createdAt - a.createdAt).splice((sp - 1) * ss, ss),
                                    total: count
                                }
                            );
                        }
                    }
                });
            }
        );
    }

    insert(c) {
        const {
            tipo, rsocial, nome, cnpj_cpf, rg_ie, endereco,
            bairro, cep, estado, cidade, telefone, celular, email
        } = c;
        
        return new Promise(
            (resolve, reject) => {
                let o = {
                    tipo:       tipo || 'FISICA',
                    rsocial:    rsocial,
                    nome:       nome,
                    cnpj_cpf:   cnpj_cpf ? cnpj_cpf.replace(/[^0-9]/g, '') : undefined,
                    rg_ie:      rg_ie,
                    endereco:   endereco,
                    bairro:     bairro,
                    cep:        cep,
                    estado:     estado,
                    cidade:     cidade,
                    telefone:   telefone,
                    celular:    celular,
                    email:      email
                };

                db.insert(o, (e, n) => {
                    if (e) {
                        logger(`Erro ao inserir cliente`, e);
                        reject(e)
                    };

                    logger(`Novo cliente inserido [${n._id}]`);
                    resolve(n);
                });
            }
        );
    }

    update(id, c) {
        const {
            tipo, rsocial, nome, cnpj_cpf, rg_ie, endereco,
            bairro, cep, estado, cidade, telefone, celular, email
        } = c;

        return new Promise(
            (resolve, reject) => {
                if (!id) {
                    logger(`Erro ao atualizar cliente [${id}]: ID não existente`);
                    reject({errorType: 'noIdSpecified' });
                }

                let o = {
                    tipo:       tipo || 'FISICA',
                    rsocial:    rsocial,
                    nome:       nome,
                    cnpj_cpf:   cnpj_cpf ? cnpj_cpf.replace(/[^0-9]/g, '') : undefined,
                    rg_ie:      rg_ie,
                    endereco:   endereco,
                    bairro:     bairro,
                    cep:        cep,
                    estado:     estado,
                    cidade:     cidade,
                    telefone:   telefone,
                    celular:    celular,
                    email:      email
                };

                db.update({ _id: id }, o, (e, n) => {
                    if (e) {
                        logger(`Erro ao atualizar cliente [${id}]`, e);
                        reject(e)
                    };

                    logger(`Cliente [${id}] atualizado`);
                    resolve(n);
                });
            }
        );
    }

    remove(id) {
        return new Promise(
            (resolve, reject) => {
                if (!id) {
                    logger(`Erro ao remover cliente [${id}]: ID não existente`);
                    reject({errorType: 'cliente_noIdGiven'});
                }

                let errors = [];

                /* Veículos */
                veiculosDb.remove({ id_cliente: id }, { multi: true }, (e, q) => {
                    if (e) {
                        logger(`Erro ao remover veículos do cliente [${id}]`, e);
                        errors.push(e);
                        reject(errors);
                    }
                });

                /* OS e seus itens */
                osDb.find({ id_cliente: id }, (e, d) => {
                    if (e) {
                        logger(`Erro ao encontrar OSs do cliente [${id}] para remoção`, e);
                        errors.push(e);
                        reject(errors);
                    } else {
                        d.forEach(os => {
                            osDb.remove(os._id)
                                .catch(e => {
                                    errors.push(e);
                                });
                        });
                    }
                });

                /* Cliente */
                this.db.remove({ _id: id }, {}, (e, q) => {
                    if (e) {
                        logger(`Erro ao remover cliente [${id}]`, e);
                        errors.push(e);
                        reject(errors);
                    }

                    logger(`Cliente [${id}] e suas ligações removidas com sucesso`);
                    resolve(q);
                });
            }
        );
    }

}