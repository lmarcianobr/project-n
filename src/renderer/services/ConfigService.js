import db from "./db/settings";
import logger from "./utils/logger";

export default class Settings {

    constructor() {
        this.db = db;
    }

    getCurrentAutoIncrementOS() {
        return new Promise(
            (resolve, reject) => {
                this.db.findOne(
                    { _id: '__os_autoinsert__' },
                    (err, data) => {
                        if (err) {
                            logger(`Erro ao obter auto incremento do código da OS`, err);
                            reject(err);
                        }
                        resolve(data);
                    }
                )
            }
        );
    }

    getAutoIncrementOS() {
        return new Promise(
            (resolve, reject) => {
                this.db.update(
                    { _id: '__os_autoinsert__' },
                    { $inc: { value: 1 } },
                    { upsert: true, returnUpdatedDocs: true },
                    (err, affected, obj) => {
                        if (err) {
                            logger(`Erro ao obter auto incremento do código da OS`, err);
                            reject(err);
                        }
                        resolve(obj.value);
                    }
                )
            }
        );
    }

    getInfoEmpresa() {
        return new Promise(
            (resolve, reject) => {
                this.db.findOne(
                    { _id: '__info_empresa__' },
                    (err, data) => {
                        if (err) {
                            logger(`Erro ao obter informações da empresa`, err);
                            reject(err);
                        }
                        resolve(data);
                    }
                )
            }
        );
    }

    setInfoEmpresa(empresa) {
        return new Promise(
            (resolve, reject) => {
                this.db.update(
                    { _id: '__info_empresa__' },
                    { $set: { value: empresa } },
                    { upsert: true },
                    (err, data) => {
                        if (err) {
                            logger(`Erro ao salvar informações da empresa`, err);
                            reject(err);
                        }

                        resolve(data);
                    }
                )
            }
        );
    }

}