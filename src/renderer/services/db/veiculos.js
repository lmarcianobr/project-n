import envs from "../../envs";
import logger from "../utils/logger";

const path  = require('path');
const Datastore = require('nedb');

const dbpath = path.join(envs[process.env.NODE_ENV], 'db', 'veiculos.db');

logger(`Iniciando base de dados de VEÍCULO em [${dbpath}]`);

const db = new Datastore({
    filename:       dbpath, 
    autoload:       true,
    timestampData:  true
});

db.ensureIndex({
    fieldName: 'placa', 
    unique: true, sparse: true
}, e => {
    if (e) { 
        console.error("Erro ao inserir índice único em Veículos.", e);
    }
});

db.persistence.setAutocompactionInterval(60000);

export default db;