import envs from "../../envs";
import logger from "../utils/logger";

const path  = require('path');
const Datastore = require('nedb');

const dbpath = path.join(envs[process.env.NODE_ENV], 'db', 'clientes.db');

logger(`Iniciando base de dados de CLIENTE em [${dbpath}]`);

const db = new Datastore({
    filename:       dbpath,
    autoload:       true,
    timestampData:  true
});

db.ensureIndex({
    fieldName: 'cnpj_cpf', 
    unique: true, sparse: true
}, e => {
    if (e) { 
        console.error("Erro ao inserir índice único em Cliente.", e);
    }
});

db.persistence.setAutocompactionInterval(60000);

export default db;