import envs from "../../envs";
import logger from "../utils/logger";

const path  = require('path');
const Datastore = require('nedb');

const dbpath = path.join(envs[process.env.NODE_ENV], 'db', 'settings.db');

logger(`Iniciando base de dados de CONFIGURAÇÕES em [${dbpath}]`);

const db = new Datastore({
    filename:       dbpath,
    autoload:       true
});

db.persistence.setAutocompactionInterval(300000);

export default db;