import envs from "../../envs";
import logger from "../utils/logger";

const path  = require('path');
const Datastore = require('nedb');

const dbpath = path.join(envs[process.env.NODE_ENV], 'db', 'os.db');

logger(`Iniciando base de dados de ORDEM DE SERVIÇO em [${dbpath}]`);

const db = new Datastore({
    filename:       dbpath, 
    autoload:       true,
    timestampData:  true
});

db.persistence.setAutocompactionInterval(60000);

export default db;