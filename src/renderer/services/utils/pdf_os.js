const remote    = require('electron').remote;
const shell     = require('electron').shell;
const dialog    = remote.dialog;
const app       = remote.app;
const path      = require('path');
const fs        = require('fs');
const pdfmake   = require('pdfmake');

import { formatCep, formatCpfCnpj, formatCurrency, formatPlate, formatTel, formatTanque } from "./utils";
import ConfigService from "../ConfigService";

export default function (os) {
    return new Promise(async (resolve, reject) => {
        try {
            let config = new ConfigService();

            let dialogOptions = {
                defaultPath: path.resolve(app.getPath('documents'), `OS_${os.codigo || os._id}.pdf`),
                filters: [{name: 'Documento PDF', extensions: ['pdf']}]
            }
        
            let user_path = dialog.showSaveDialog(dialogOptions);
            
            if (user_path) {
                let fonts = {
                    Nunito: {
                        normal:         path.join(__static, '/Nunito-Regular.ttf'),
                        bold:           path.join(__static, '/Nunito-Bold.ttf'),
                        italics:        path.join(__static, '/Nunito-Italic.ttf'),
                        bolditalics:    path.join(__static, '/Nunito-BoldItalic.ttf')
                    },
                };

                const printer = new pdfmake(fonts);

                let config_empresa = await config.getInfoEmpresa();
                let empresa = config_empresa.value;

                let itens_body = 
                [
                    [
                        {text: 'Quantidade', bold: true, alignment: 'center'},
                        {text: 'Discriminação', bold: true},
                        {text: 'Valor Unitário', bold: true, alignment: 'right'},
                        {text: 'Valor Total', bold: true, alignment: 'right'},
                    ]
                ].concat(
                    os.itens.map(e => 
                        [
                            {text: e.qtd, alignment: 'center'},
                            e.descricao,
                            {text: formatCurrency(e.valorunit), alignment: 'right'},
                            {text: formatCurrency(e.qtd * e.valorunit), alignment: 'right'}
                        ]
                    )
                );

                let logo_row = empresa.logotipo ?
                    {rowSpan: 2, image: empresa.logotipo, width: 90, height: 90, alignment: 'center', border: [true, true, false, true]} :
                    {rowSpan: 2, text: '', border: [true, true, false, true]};
        
                let doc_def = {
                    content: [
                        {
                            table: {
                                widths: [100, '*'],
                                heights: ['*', 60],
                                body: [
                                    [
                                        logo_row,
                                        {text: empresa.nome || empresa.rsocial, fontSize: 20, bold: true, border: [false, true, true, false]}
                                    ],
                                    [   {text: '', border: [false, false, false, false]},
                                        {
                                            border: [false, false, true, true], bold: true,
                                            text:
                                            [
                                                [empresa.endereco, empresa.bairro].filter(Boolean).join(' - '),
                                                [formatCep(empresa.cep), empresa.cidade, empresa.estado].filter(Boolean).join(' - '),
                                                [formatTel(empresa.telefone), formatTel(empresa.celular)].filter(Boolean).join(' - '),
                                                [formatCpfCnpj(empresa.cnpj), empresa.ie].filter(Boolean).join(' - '),
                                                
                                            ].filter(Boolean).join('\n')
                                        }
                                    ]
                                ]
                            }
                        },
                        {
                            margin: [0, 10, 0, 0],
                            fontSize: 12, bold: true, alignment: 'center',
                            text: `ORDEM DE SERVIÇO Nº ${os.codigo}`
                        },
                        {
                            margin: [0, 10, 0, 0],
                            fontSize: 10, bold: true,
                            text: 'DADOS DO SOLICITANTE'  
                        },
                        {
                            margin: [0, 3, 0, 0],
                            table: {
                                widths: ['auto', '*'],
                                body: [
                                    [
                                        {text: "Nome:", bold: true, border: [true, true, false, true]}, 
                                        {text: os.cliente.rsocial || os.cliente.nome, border: [false, true, true, true]}
                                    ],
                                    [
                                        {text: "Endereço:", bold: true, border: [true, true, false, true]},
                                        {
                                            text: [os.cliente.endereco, os.cliente.bairro, os.cliente.cidade, os.cliente.estado].filter(Boolean).join(' - '),
                                            border: [false, true, true, true]
                                        }
                                    ],
                                    [
                                        {text: "Telefone:", bold: true, border: [true, true, false, true]}, 
                                        {
                                            text: [formatTel(os.cliente.telefone), formatTel(os.cliente.celular)].filter(Boolean).join(' / '),
                                            border: [false, true, true, true]
                                        }
                                    ],
                                ]
                            }
                        },
                        {
                            margin: [0, 10, 0, 0],
                            fontSize: 10, bold: true,
                            text: 'DADOS DO VEÍCULO'  
                        },
                        {
                            margin: [0, 3, 0, 0],
                            table: {
                                widths: [90, '*', 'auto', 100],
                                body: [
                                    [
                                        {text: "Marca / Modelo:", bold: true, border: [true, true, false, true]}, 
                                        {text: os.veiculo ? [os.veiculo.marca, os.veiculo.modelo].filter(Boolean).join(' / ') : '', border: [false, true, true, true]},
                                        {text: "Placa:", bold: true, border: [true, true, false, true]}, 
                                        {text: os.veiculo ? formatPlate(os.veiculo.placa) : '', border: [false, true, true, true]},
                                    ],
                                    [
                                        {text: "Quilometragem:", bold: true, border: [true, true, false, true]}, 
                                        {text: Number(os.quilometragem).toLocaleString(), border: [false, true, true, true]},
                                        {text: "Tanque:", bold: true, border: [true, true, false, true]}, 
                                        {text: formatTanque(os.tanque), border: [false, true, true, true]},
                                    ],
                                ]
                            }
                        },
                        {
                            margin: [0, 10, 0, 0],
                            fontSize: 10, bold: true,
                            text: 'ITENS'  
                        },
                        {
                            margin: [0, 3, 0, 0],
                            table: {
                                widths: [70, '*', 80, 80],
                                body: itens_body
                            }
                        },
                        {
                            margin: [0, 10, 0, 0],
                            table: {
                                widths: ['*', 169],
                                body: [
                                    [
                                        {text: 'Valor total dos itens', bold: true},
                                        {text: formatCurrency(os.itens.reduce((ac, cv) => ac + (cv.qtd * cv.valorunit), 0)), bold: true, alignment: 'right'}
                                    ]
                                ]
                            }  
                        },
                    ],
                    footer: function(currentPage, pageCount) { return [{text: `Página ${currentPage} de ${pageCount}`, alignment: 'center'}]; },
                    defaultStyle: {
                        font: 'Nunito'
                    }
                };
        
                let doc = printer.createPdfKitDocument(doc_def);
                doc.pipe(fs.createWriteStream(user_path));
                doc.end();
                shell.openItem(user_path);
                resolve(true);
            } else {
                resolve(false);
            }
        } catch (e) {
            console.error("Erro ao gerar PDF da ordem de serviço.", e);
            reject(e);
        }
    });
}
