import envs from "../../envs";

const fs = require('fs');
const path  = require('path');

function toJSONLocal (date) {
    var local = new Date(date);
    local.setMinutes(date.getMinutes() - date.getTimezoneOffset());
    return local.toJSON().slice(0, 10);
}

export default function(...text) {
    let filepath = path.join(envs[process.env.NODE_ENV], 'logs', `log_${toJSONLocal(new Date())}.txt`);
    let date = new Date().toLocaleString();

    // fs.open(filepath, 'w', function () {});

    fs.appendFile(
        filepath,
        text.reduce((a, c) => a.concat(`[${date}] ${c}\n`), ''),
        e => {
            if (e) {
                console.error("Erro a escrever o log.", e);
            }
        }
    );
}