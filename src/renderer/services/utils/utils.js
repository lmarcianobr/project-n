export const debounce = (func, wait, immediate) => {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

export const formatTel = t => {
    if (typeof t == 'string' && t.length == 11) { // (XX) XXXXX-XXXX
        return `(${t.substr(0, 2)}) ${t.substr(2, 5)}-${t.substr(7, 4)}`;
    } else if (typeof t == 'string' && t.length == 10) { // (XX) XXXX-XXXX
        return `(${t.substr(0, 2)}) ${t.substr(2, 4)}-${t.substr(6, 4)}`;
    } else {
        return t;
    }
}

export const formatCpfCnpj = t => {
    if (typeof t == 'string' && t.length == 14) { // XX.XXX.XXX/XXXX-XX
        return `${t.substr(0, 2)}.${t.substr(2, 3)}.${t.substr(5, 3)}/${t.substr(8, 4)}-${t.substr(12, 2)}`;
    } else if (typeof t == 'string' && t.length == 11) { // XXX.XXX.XXX-XX
        return `${t.substr(0, 3)}.${t.substr(3, 3)}.${t.substr(6, 3)}-${t.substr(9, 2)}`;
    } else {
        return t;
    }
}

export const formatPlate = t => {
    if (typeof t == 'string' && t.replace(/[\-]/g, '').length == 7) {
        let p = t.toLocaleUpperCase();
        return `${p.substr(0, 3)}-${p.substr(3, 4)}`;
    } else {
        return t;
    }
}

export const formatCurrency = t => {
    let n = Number(t);
  
    if (!isNaN(n)) {
        return `R$ ${n.toLocaleString('pt-BR', {minimumFractionDigits: 2, maximumFractionDigits: 2})}`;
    } else {
        return t;
    }
}

export const formatCep = t => {
    if (typeof t == 'string' && t.replace(/[\-|\.]/g, '').length == 8) {
        return `${t.substr(0, 2)}.${t.substr(2, 3)}-${t.substr(5, 3)}`;
    } else {
        return t;
    }
}

export const formatTanque = l => {
	switch (l) {
		case 0:
			return "Reserva";
		case 1:
			return "1/4";
		case 2:
			return "1/2";
		case 3:
			return "3/4";
		case 4:
			return "Cheio";
	}                
}