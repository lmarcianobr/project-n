import db from "./db/veiculos";
import logger from "./utils/logger";
import clienteDb from "./db/clientes";
import osDb from "./db/os";

const fuzzysort = require('fuzzysort');

export default class Veiculos {

    constructor() {
        this.db = db;
    }

    searchVeiculos(query = '', page = 1, size = 20) {
        /* Sanitize query */
        let sq = (typeof query == 'string') ? query.toLocaleLowerCase().trim().replace(/[-|/|.|(|)]/g, '') : '';

        /* Sanitize page */
        let sp = Number(page); sp = isNaN(sp) ? 1 : (sp < 1 ? 1 : sp);

        /* Sanitize size */
        let ss = Number(size); ss = isNaN(ss) ? 20 : (ss < 1 ? 1 : ss);

        return new Promise(
            (resolve, reject) => {
                this.db.find(
                    {}, { marca: 1, modelo: 1, cor: 1, placa: 1, id_cliente: 1, createdAt: 1, _id: 0 },
                    async (err, data) => {
                        try {
                            if (err) throw err;

                            data = data.sort((a, b) => a.createdAt - b.createdAt);
                            
                            for (const veiculo of data) {
                                veiculo.cliente = await new Promise((res, rej) => {
                                    clienteDb.findOne(
                                        { _id: veiculo.id_cliente },
                                        { nome: 1 },
                                        (error, cliente) => {
                                            if (error) rej(error)
                                            res(cliente);
                                        }
                                    )
                                });
                            }

                            let ret = {
                                total: 0,
                                content: []
                            }

                            if (sq) {
                                let content = fuzzysort.go(sq, data, { keys: ['marca', 'modelo', 'cor', 'placa', 'cliente.nome'] }).map(e => e.obj);
                                
                                ret.total = content.length;
                                ret.content = content.splice((sp - 1) * ss, ss);
                            } else {
                                ret.total = data.length;
                                ret.content = data.splice((sp - 1) * ss, ss);
                            }
                            
                            resolve(ret);
                        } catch (e) {
                            reject(e);
                        }
                    }
                )
            }
        );
    }

    getByCliente(id) {
        return new Promise(
            (resolve, reject) => {
                this.db.find(
                    { id_cliente: id },
                    (ev, dv) => {
                        if (ev) {
                            logger(`Erro ao obter veículos do cliente [${id}]`, ev);
                            reject(ev)
                        }
                        resolve(dv);
                    }
                );
            }
        );
    }

    get(id) {
        return new Promise(
            (resolve, reject) => {
                this.db.findOne(
                    { _id: id },
                    (ev, dv) => {
                        if (ev) {
                            logger(`Erro ao obter veículo [${id}]`, ev);
                            reject(ev);
                        }
                        resolve(dv);
                    }
                );
            }
        );
    }

    insert(c) {
        const {id_cliente, marca, modelo, cor, placa} = c;
        
        return new Promise(
            (resolve, reject) => {
                if (!id_cliente) {
                    logger(`Erro ao inserir veículo: Não há cliente para associar`);
                    reject({errorType: 'notNullViolated'});
                }

                let v = {
                    id_cliente: id_cliente,
                    marca:      marca   || undefined,
                    modelo:     modelo  || undefined,
                    cor:        cor     || undefined,
                    placa:      placa.replace('-', '')   || undefined
                };

                db.insert(v, (e, n) => {
                    if (e) {
                        logger(`Erro ao inserir veículo no cliente [${id_cliente}]`, e);
                        reject(e);
                    }

                    logger(`Veículo [${n._id}] inserido com sucesso`);
                    resolve(n);
                });
            }
        );
    }

    update(id, c) {
        const {id_cliente, marca, modelo, cor, placa} = c;
        
        return new Promise(
            (resolve, reject) => {
                if (!id_cliente) {
                    logger(`Erro ao atualizar veículo: Não há cliente para associar`);
                    reject({errorType: 'notNullViolated'});
                }

                let v = {
                    id_cliente: id_cliente,
                    marca:      marca   || undefined,
                    modelo:     modelo  || undefined,
                    cor:        cor     || undefined,
                    placa:      placa.replace('-', '')   || undefined
                };

                db.update({ _id: id }, v, (e, n) => {
                    if (e) {
                        logger(`Erro ao atualizar veículo no cliente [${id_cliente}]`, e);
                        reject(e);
                    }

                    logger(`Veículo [${id}] atualizado com sucesso`);
                    resolve(n);
                });
            }
        );
    }

    remove(id) {
        return new Promise(
            async (resolve, reject) => {
                if (!id) {
                    logger(`Erro ao remover veículo: ID nulo`);
                    reject({errorType: 'noIdGiven'});
                }

                let osComVeiculo = await new Promise((rs, rj) => {
                    osDb.find(
                        { id_veiculo: id }, { codigo: 1 },
                        (e, d) => {
                            if (e) rj(e);
                            rs(d);
                        }
                    )
                });

                if (osComVeiculo && osComVeiculo.length > 0) {
                    reject(
                        {
                            errorType: 'hasDependencies',
                            data: osComVeiculo
                        }
                    );
                    return;
                }

                db.remove({ _id: id }, {}, (e, q) => {
                    // e: erro
                    // q: qtde removida
                    if (e) {
                        logger(`Erro ao remover veículo [${id}]`, e);
                        reject(e);
                    }

                    logger(`Veículo [${id}] removido com sucesso`);
                    resolve(q);
                })
            }
        );
    }
}