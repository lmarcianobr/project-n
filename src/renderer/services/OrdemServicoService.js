import db from "./db/os";

import ClienteService from "./ClienteService";
import VeiculoService from "./VeiculoService";
import ItemOrdemServicoService from "./ItemOrdemServicoService";
import ConfigService from "./ConfigService";

import logger from "./utils/logger";

export default class OrdemServico {

    constructor() {
        this.db = db;
        this.clienteService = new ClienteService();
        this.veiculoService = new VeiculoService();
        this.itemService = new ItemOrdemServicoService();
        this.configService = new ConfigService();
    }

    findAll(query = '', page = 1, size = 20, column = 'createdAt', order = 'asc') {
        /* Sanitize query */
        let sq = (typeof query == 'string') ? query.toLocaleLowerCase().trim().replace(/[-|/|.|(|)]/g, '') : '';

        /* Sanitize page */
        let sp = Number(page); sp = isNaN(sp) ? 1 : (sp < 1 ? 1 : sp);

        /* Sanitize size */
        let ss = Number(size); ss = isNaN(ss) ? 20 : (ss < 1 ? 1 : ss);

        /* Sanitize column */
        // let sc = (typeof column == 'string') ? column.toLocaleLowerCase().trim() : 'createdAt';

        /* Sanitize order */
        let so = order == 'asc' ? 1 : -1;

        return new Promise(
            (resolve, reject) => {
                db.find({}, async (e, d) => {
                    if (e) { 
                        logger(`Erro ao pesquisar OS`, e);
                        reject(e);
                    }

                    let results = [];

                    for (const os of d) {
                        let c_id = os.id_cliente;
                        let v_id = os.id_veiculo;
                        let search_terms = `${os.codigo} `;

                        if (c_id) {
                            os.cliente = await this.clienteService.get(c_id, false);
                            search_terms = search_terms.concat(`${os.cliente.nome} ${os.cliente.rsocial} ${os.cliente.telefone || ''} ${os.cliente.celular || ''} ${os.cliente.cnpj_cpf || ''} `);
                        }

                        if (v_id) {
                            os.veiculo = await this.veiculoService.get(v_id);

                            if (os.veiculo) {
                                search_terms = search_terms.concat(`${os.veiculo.marca || ''} ${os.veiculo.modelo || ''} ${os.veiculo.placa || ''}`);
                            }
                        }

                        if (search_terms.toLocaleLowerCase().includes(sq.toLocaleLowerCase())) {
                            results.push(os);
                        }
                    }
                    
                    let count = results.length;

                    /* Sort */
                    if (so > 0) {   /* ASC */
                        resolve(
                            {
                                content: results.sort((a, b) => a.createdAt - b.createdAt).splice((sp - 1) * ss, ss),
                                total: count
                            }
                        );
                    } else {        /* DESC */
                        resolve(
                            {
                                content: results.sort((a, b) => b.createdAt - a.createdAt).splice((sp - 1) * ss, ss),
                                total: count
                            }
                        );
                    }
                });
            }
        );
    }

    get(id) {
        return new Promise(
            (resolve, reject) => {
                db.findOne(
                    {_id: id}, 
                    async (error, data) => {
                        if (error) {
                            logger(`Erro ao obter OS [${id}]`, e);
                            reject(error)
                        }

                        if (data.id_cliente) {
                            data.cliente = await this.clienteService.get(data.id_cliente, false);
                        }

                        if (data.id_veiculo) {
                            data.veiculo = await this.veiculoService.get(data.id_veiculo);
                        }

                        data.itens = await this.itemService.getItemsByOs(data._id);

                        resolve(data);
                    }
                )
            }
        );
    }

    insert(os) {
        const {
            cliente, contato, quilometragem, status,
            tanque, telefone, veiculo, itens, observacao
        } = os;
        
        return new Promise(
            async (resolve, reject) => {
                let cod = await this.configService.getAutoIncrementOS();

                let o = {
                    codigo:         cod,
                    id_cliente:     cliente ? cliente._id : null,
                    contato:        contato,
                    quilometragem:  quilometragem,
                    status:         status,
                    tanque:         tanque,
                    telefone:       telefone,
                    observacao:     observacao,
                    id_veiculo:     veiculo ? veiculo._id : null
                };

                db.insert(o, (error, data) => {
                    if (error) {
                        logger(`Erro ao inserir OS`, error);
                        reject(error)
                    }

                    logger(`OS [${data._id}] inserida com sucesso`);
                    
                    this.itemService.insertItems(data._id, itens)
                        .then(r => {
                            data.itens = r;
                            resolve(data);
                        })
                        .catch(e => {
                            reject(e);
                        });
                });
            }
        );
    }

    update(id, os) {
        const {
            codigo, cliente, contato, status, quilometragem, 
            tanque, telefone, veiculo, itens, observacao
        } = os;

        return new Promise(
            (resolve, reject) => {
                if (!id) {
                    logger(`Erro ao atualizar OS [${id}]: ID da OS não existe`);
                    reject({errorType: 'OS_noIdSpecified' });
                }

                let o = {
                    codigo:         codigo,
                    id_cliente:     cliente ? cliente._id : null,
                    contato:        contato,
                    quilometragem:  quilometragem,
                    status:         status,
                    tanque:         tanque,
                    telefone:       telefone,
                    observacao:     observacao,
                    id_veiculo:     veiculo ? veiculo._id : null
                };

                db.update({ _id: id }, o, (e, n) => {
                    if (e) {
                        logger(`Erro ao atualizar OS [${id}]: ID da OS não existe`);
                        reject(e)
                    }

                    logger(`OS [${id}] atualizada com sucesso`);

                    this.itemService.updateItems(id, itens)
                        .then(r => {
                            resolve(os);
                        })
                        .catch(e => {
                            reject(e);
                        });
                });
            }
        );
    }

    remove(id) {
        return new Promise(
            (resolve, reject) => {
                if (!id) {
                    logger(`Erro ao remover OS [${id}]: ID da OS não existe`);
                    reject({errorType: 'noIdGiven'});
                }

                db.remove({ _id: id }, {}, (e, q) => {
                    if (e) {reject(e)};

                    logger(`OS [${data._id}] removida com sucesso`);

                    this.itemService.removeAllByOs(id)
                        .then(_ => {
                            resolve();
                        });
                })
            }
        );
    }
}