import clientes from "./db/clientes";
import veiculos from "./db/veiculos";
import os from "./db/os";

export default class IndicadorService {

    clientesCadastrados() {
        return new Promise((resolve, reject) => {
            clientes.count(
                {},
                (err, count) => {
                    try {
                        if (err) throw err;
                        resolve(count);
                    } catch (e) {
                        reject(e);
                    }
                }
            );
        });
    }

    veiculosCadastrados() {
        return new Promise((resolve, reject) => {
            veiculos.count(
                {},
                (err, count) => {
                    try {
                        if (err) throw err;
                        resolve(count);
                    } catch (e) {
                        reject(e);
                    }
                }
            );
        });
    }

    OShoje() {
        return new Promise((resolve, reject) => {
            os.count(
                { 
                    $where: function() {
                        return this.createdAt.toLocaleDateString() == (new Date).toLocaleDateString();
                    }
                },
                (err, count) => {
                    try {
                        if (err) throw err;
                        resolve(count);
                    } catch (e) {
                        reject(e);
                    }
                }
            );
        });
    }

    OSmesAtual() {
        return new Promise((resolve, reject) => {
            os.count(
                { 
                    $where: function() {
                        let date = new Date();
                        let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
                        let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0, 23, 59, 59);

                        return (this.createdAt > firstDay) && (this.createdAt < lastDay);
                    }
                },
                (err, count) => {
                    try {
                        if (err) throw err;
                        resolve(count);
                    } catch (e) {
                        reject(e);
                    }
                }
            );
        });
    }

}