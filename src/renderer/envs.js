const app   = require('electron').remote.app;
const path  = require('path');

export default {
    development: "",
    production: path.join(app.getPath("documents"), 'OSMecanica')
}