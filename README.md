# OS Mecânica

---

Para gerar aplicação em 32-bits:

* Alterar `arch` em `.electron-vue/build.config.js`,  de `x64` para `ia32`;
* `npm run build:win32` e `npm run setup:ia32`

Lembrar de alterar `arch` novamente para `x64` para poder utilizar o comando `npm run setup:win32` e gerar a aplicação em 64-bits.

Download: https://mega.nz/file/rZV0hAbK#kUSfQysQG9GfIinf9PJTx0eTZgFC3zIwI0yudOKVhn8